def getGoPath() {
  return "${env.WORKSPACE}/build"
}

def getWorkspacePath() {
  return readFile("${env.WORKSPACE}/workspace_path.txt").trim()
}

pipeline {
    agent any
    tools {
        go("go-1.9")
    }

    stages {
        stage("Checkout") {
            steps {
              script {
                def goPath = getGoPath()
                def path = getWorkspacePath()
                dir("${goPath}/src/${path}") {
                  checkout scm
                  sh("git reset --hard")
                  sh("git clean -fdx")
                }
              }
            }
        }

        stage("Build") {
            steps {
              script {
                def goPath = getGoPath()
                def path = getWorkspacePath()
                dir("${goPath}/src/${path}") {
                  withEnv(["GOPATH=${goPath}", "PATH+GO=${goPath}/bin"]) {
                    sh("make package")
                  }
                }
              }
            }
        }
    }

    post {
        always {
            archiveArtifacts(artifacts: "**/build/packages/*.tar", fingerprint: true,  onlyIfSuccessful: true)
        }
    }
}