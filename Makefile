VERSION?=$(shell git describe --tags)
COMMIT:=$(shell git rev-parse HEAD)

BINARY:=go-jarsigner

CMD_DIR:=cmd/${BINARY}
WEB_DIR:=web

BUILD_DIR:=$(shell pwd)/build
BINARY_DIR:=${BUILD_DIR}/binaries
WEB_BUILD_DIR:=${BUILD_DIR}/web
PACKAGE_DIR:=${BUILD_DIR}/packages

# Setup the -ldflags option for go build here, interpolate the variable values
LDFLAGS:=-ldflags "-s -w -X main.Version=${VERSION} -X main.Commit=${COMMIT}"

# Cross compilation
OS:=-os="linux darwin windows"
OS_ARCH=-arch="amd64" 

clean:
	-rm -rf ${BUILD_DIR} vendor ${WEB_DIR}/dist ${WEB_DIR}/node_modules download tmp

setup_workspace:
	mkdir -p ${BUILD_DIR} ${WEB_BUILD_DIR} ${PACKAGE_DIR} ${BINARY_DIR}

get_dependencies:
	go get github.com/mitchellh/gox \
				 github.com/golang/dep/cmd/dep; \

	dep ensure

web: setup_workspace
	cd ${WEB_DIR}; \
	yarn install; \
	yarn build; \
	mv dist ${WEB_BUILD_DIR}; \
	cp index.html ${WEB_BUILD_DIR}
	cd - > /dev/null

web_dev: setup_workspace
	cd ${WEB_DIR}; \
	yarn install; \
	yarn build; \
	cd - > /dev/null

go-tools:
	- go fmt $$(go list ./... | grep -v /vendor/)
	- go vet -v $$(go list ./... | grep -v /vendor/)

build: setup_workspace get_dependencies web go-tools
	cd ${BINARY_DIR}; \
	gox -verbose ${OS} ${OS_ARCH} ${LDFLAGS} ../../${CMD_DIR}; \
	cd - > /dev/null

package: build
	cd ${PACKAGE_DIR}; \
	for binary in `find ${BUILD_DIR} -type f -name ${BINARY}*`; \
	do \
		filename=$$(basename $$binary); \
		tar cf $$filename.tar -C ${BUILD_DIR} web -C ${BINARY_DIR} $$filename; \
	done
	cd - > /dev/null

dev: get_dependencies go-tools web_dev
	go run ${CMD_DIR}/main.go -alias=${alias} -digestalg=${digestalg} -keypass=${keypass} -keystore=${keystore} -storepass=${storepass}

.PHONY: package build web clean setup_workspace get_dependencies go-tools dev