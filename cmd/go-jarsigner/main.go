package main

import (
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"

	"github.com/google/uuid"
	"github.com/gorilla/mux"

	"gitlab.com/aaronvaz/jar-signer-app/pkg/dispatcher"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/loggers"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/serverhandlers"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/utils"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/ws"
)

var (
	// Version represents the version of the app
	Version = "1.0.0"

	// Commit represents the git commit of the binary
	Commit = "dev"
)

// ServerHandler struct
type ServerHandler struct {
	logger *loggers.ServerLogger
	mux    *mux.Router
}

// AppServerHandler creates a new Server handler object for the app
func AppServerHandler() *ServerHandler {
	server := &ServerHandler{
		logger: loggers.AppLogger,
		mux:    mux.NewRouter(),
	}

	server.mux.HandleFunc("/upload/{id}", serverhandlers.UploadHandler).Methods("POST")
	server.mux.HandleFunc("/payload/{id}", ws.MessageHandler)
	server.mux.PathPrefix("/download").HandlerFunc(ws.DownloadHandler)
	server.mux.PathPrefix("/").Handler(http.FileServer(http.Dir("./web")))
	return server
}

func (s *ServerHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.logger.Printf("%s %s %s", r.RemoteAddr, r.Method, r.RequestURI)

	// create session cookie if it doesn't already exist
	if _, err := r.Cookie(utils.SessionCookieName); err != nil {
		s.createCookie(w)
	}
	s.mux.ServeHTTP(w, r)

	defer utils.CatchErr()
}

func (s *ServerHandler) createCookie(w http.ResponseWriter) {
	sessionID := &http.Cookie{
		Name:  utils.SessionCookieName,
		Value: uuid.New().String(),
	}
	http.SetCookie(w, sessionID)
}

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	checkForExecutable(utils.JarSignerExe)
	verifyKeyStore(utils.AppArgs.Keystore)
}

func main() {
	// start dispatcher
	go dispatcher.StartDispatcher()

	loggers.AppLogger.Println("Listening on http://0.0.0.0:" + utils.AppArgs.Port)
	loggers.AppLogger.Fatal(http.ListenAndServe(":"+utils.AppArgs.Port, AppServerHandler()))
}

func checkForExecutable(executable string) {
	location, err := exec.LookPath(executable)
	if err != nil {
		loggers.AppLogger.Printf("%s not found on path. Please install a valid Java JDK and try again", utils.JarSignerExe)
		loggers.AppLogger.Fatalln(err)
	}
	loggers.AppLogger.Printf("%s found at %s", utils.JarSignerExe, location)
}

func verifyKeyStore(keystore string) {
	if filepath.IsAbs(keystore) {
		if _, err := os.Stat(keystore); os.IsNotExist(err) {
			loggers.AppLogger.Fatalln(err)
		}
		loggers.AppLogger.Printf("keystore %s found in %s", keystore, filepath.Dir(keystore))
	} else {
		// check to see if keystore is in the working dir first
		cwdKeystore := filepath.Join(utils.AppArgs.WorkingDir, keystore)
		if _, err := os.Stat(cwdKeystore); os.IsNotExist(err) {
			loggers.AppLogger.Fatalf(`%s not found in supplied or in the current working dir.
						Please use an absolute path or place keystore in the same directory as the binary`, keystore)
		}
		loggers.AppLogger.Printf("keystore %s found in %s", keystore, utils.AppArgs.WorkingDir)
		utils.AppArgs.Keystore = cwdKeystore
	}
}
