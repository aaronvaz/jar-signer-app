package cleaner

import (
	"os"
	"path/filepath"
	"regexp"
	"time"

	"gitlab.com/aaronvaz/jar-signer-app/pkg/loggers"

	"gitlab.com/aaronvaz/jar-signer-app/pkg/utils"
)

var (
	// CleanupSchedule is a ticker that ticks every 4 hours to run the cleanup task
	CleanupSchedule = time.NewTicker(4 * time.Hour)

	createdDateRegex = regexp.MustCompile(`(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z(\d{2}:\d{2})?)`)
)

// CleanupJob represents a job that will cleanup old signed zips
// zips will only be kept for a day, any zips over a day will be cleaned by the job
type CleanupJob struct {
}

// Run starts the CleanupJob
func (c *CleanupJob) Run() {
	if _, err := os.Stat(utils.DownloadDir); os.IsNotExist(err) {
		return
	}

	loggers.AppLogger.Println("Starting cleanup job")

	expiredZips := collectZips()
	if len(expiredZips) > 0 {
		loggers.AppLogger.Printf("%d zips will be cleaned", len(expiredZips))
		for _, zip := range expiredZips {
			err := os.Remove(zip)
			utils.ErrCheck(err)
		}
	}

	loggers.AppLogger.Println("No expired zips found")
}

func collectZips() []string {
	expiredZips := make([]string, 0)

	err := filepath.Walk(utils.DownloadDir, func(path string, info os.FileInfo, fileErr error) error {
		if info.IsDir() {
			return nil
		}

		if filepath.Ext(path) != ".zip" {
			return nil
		}

		match := createdDateRegex.FindStringSubmatch(path)
		if len(match) == 0 {
			return nil
		}

		createdDate, err := time.Parse(time.RFC3339, match[0])
		if err != nil {
			loggers.AppLogger.Println(fileErr)
			return nil
		}

		duration := time.Until(createdDate)
		if duration.Hours() > 24 {
			expiredZips = append(expiredZips, path)
		}

		return nil

	})
	utils.ErrCheck(err)

	return expiredZips
}
