package dispatcher

import (
	"gitlab.com/aaronvaz/jar-signer-app/pkg/cleaner"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/loggers"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/signing"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/ws"
)

// StartDispatcher polls the job channels and executes the received jobs
func StartDispatcher() {
	for {
		select {
		case job := <-signing.JobQueue:
			loggers.AppLogger.Printf("Starting job %s which has %d jar(s)", job.ID, job.Length)
			go job.Run()
		case output := <-ws.MessageQueue:
			go ws.MessageService.SendMessage(output.ID, output.Message)
		case zip := <-signing.ZipQueue:
			loggers.AppLogger.Printf("Building zip for %s", zip.Job.ID)
			go zip.Run()
		case download := <-ws.DowloadQueue:
			loggers.AppLogger.Printf("Building links for %s", download.ID)
			go download.Run()
		case <-cleaner.CleanupSchedule.C:
			cleanup := &cleaner.CleanupJob{}
			go cleanup.Run()
		}
	}
}
