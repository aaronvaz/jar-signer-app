package loggers

import (
	"log"
	"os"
)

// ServerLogger embeds log.Logger
// it is used to give other packages access to the server logger
type ServerLogger struct {
	*log.Logger
}

// AppLogger singleton
var AppLogger = &ServerLogger{log.New(os.Stdout, "", log.LstdFlags|log.Lshortfile)}
