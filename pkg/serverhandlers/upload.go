package serverhandlers

import (
	"io"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/mux"

	"gitlab.com/aaronvaz/jar-signer-app/pkg/signing"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/utils"
)

// UploadHandler is a http handler that handles file uploads
func UploadHandler(w http.ResponseWriter, r *http.Request) {
	upload(mux.Vars(r)["id"], w, r)
}

func upload(id string, w http.ResponseWriter, r *http.Request) {
	reader, err := r.MultipartReader()
	utils.ErrCheck(err)

	job := &signing.Job{ID: id}

	//copy each part to destination
	for {
		part, err := reader.NextPart()
		if err == io.EOF {
			break
		}

		//if part.FileName() is empty, skip this iteration.
		filename := filepath.Base(part.FileName())
		if filename == "" || filepath.Ext(filename) != ".jar" {
			continue
		}

		dstPath := filepath.Join(utils.TmpDir, job.ID)
		dst := prepareDestination(filepath.Join(dstPath, filename))
		defer utils.Close(dst)

		_, err = io.Copy(dst, part)
		utils.ErrCheck(err)

		job.Length++
	}
	signing.JobQueue <- job
}

func prepareDestination(path string) *os.File {
	parent := filepath.Dir(path)
	err := os.MkdirAll(parent, os.ModePerm)
	utils.ErrCheck(err)

	dest, err := os.Create(path)
	utils.ErrCheck(err)

	return dest
}
