package signing

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"gitlab.com/aaronvaz/jar-signer-app/pkg/loggers"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/utils"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/ws"
)

// JobQueue holds all the jobs that are being requested
var JobQueue = make(chan *Job, utils.AppArgs.MaxJobs)

// Job represents a jar signing request from the user
type Job struct {
	ID     string
	Length int
}

func (j *Job) getJars() (string, []os.FileInfo) {
	sourcePath := filepath.Join(utils.TmpDir, j.ID)
	unsignedJars, err := ioutil.ReadDir(sourcePath)
	utils.ErrCheck(err)

	return sourcePath, unsignedJars
}

// Run executes the Job
func (j *Job) Run() {
	// keep track of the jars that have been signed
	jarsCompleted := 0
	progress := make(chan int)

	defer utils.CatchErr()

	// get the jars to sign
	sourcePath, files := j.getJars()

	// process files in chunks to less load
	filesListChunks := chunk(files, 10)

	for _, chunks := range filesListChunks {
		for _, file := range chunks {
			// create command
			args := utils.AppArgs.CmdArgs(file.Name())
			cmd := exec.Command(utils.JarSignerExe, args...)

			signJarCmd := &SignJarCmd{
				job:        j,
				workDir:    sourcePath,
				jarName:    file.Name(),
				doneSignal: progress,
				command:    cmd,
			}

			go signJarCmd.Execute()
		}
	}

	// wait for job to complete
	for range progress {
		if jarsCompleted++; jarsCompleted == len(files) {
			ZipQueue <- &ZipJob{Job: j, Timestamp: time.Now()}
			break
		}
	}

	// close channel when done
	close(progress)
}

func chunk(buf []os.FileInfo, lim int) [][]os.FileInfo {
	var chunk []os.FileInfo
	chunks := make([][]os.FileInfo, 0, len(buf)/lim+1)
	for len(buf) >= lim {
		chunk, buf = buf[:lim], buf[lim:]
		chunks = append(chunks, chunk)
	}
	if len(buf) > 0 {
		chunks = append(chunks, buf[:len(buf)])
	}
	return chunks
}

// SignJarCmd is used to secute a command for a Job
// it wraps the exec.Cmd that will be used to sign a single jar
type SignJarCmd struct {
	job        *Job
	workDir    string
	jarName    string
	doneSignal chan int
	command    *exec.Cmd
}

// Execute setups up a command from a Job and executes the wrapped exec.Cmd
func (sjc *SignJarCmd) Execute() {
	if sjc.command != nil {
		sjc.command.Dir = sjc.workDir

		// execute the cmd
		logs, err := sjc.command.CombinedOutput()
		if err != nil {
			loggers.AppLogger.Printf("Failed to sign %s: %v", sjc.jarName, err)
			sjc.doneSignal <- 1
		}
		sjc.doneSignal <- 1

		go func() {
			logMessage := &ws.LogMessagesJob{
				ID: sjc.job.ID,
				Message: &ws.LogMessages{
					MessageType: "logs",
					JarName:     sjc.jarName,
					Message:     fmt.Sprintf("%s", logs),
				},
			}
			ws.MessageQueue <- logMessage
		}()
	}
}
