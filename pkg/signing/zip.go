package signing

import (
	"archive/zip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/aaronvaz/jar-signer-app/pkg/loggers"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/utils"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/ws"
)

// ZipQueue is a channel used to queue ZipJobs
var ZipQueue = make(chan *ZipJob)

// ZipJob represents a request to zip the signed jars
type ZipJob struct {
	Job       *Job
	Timestamp time.Time
}

func (z *ZipJob) filename() string {
	return fmt.Sprintf("signed-%s.zip", z.Timestamp.Format(time.RFC3339))
}

// Run executes the ZipJob
func (z *ZipJob) Run() {
	defer utils.CatchErr()

	dest := filepath.Join(utils.DownloadDir, z.Job.ID)
	err := os.MkdirAll(dest, os.ModePerm)
	utils.ErrCheck(err)

	z.createZip(dest)
	loggers.AppLogger.Printf("Zip %s created for job %s", z.filename(), z.Job.ID)

	downloadJob := &ws.DownloadJob{
		ID: z.Job.ID,
	}
	ws.DowloadQueue <- downloadJob
}

func (z *ZipJob) createZip(downloadDir string) {
	// create zip to write to
	destPath := filepath.Join(downloadDir, z.filename())
	destZip, err := os.Create(destPath)
	utils.ErrCheck(err)
	defer utils.Close(destZip)

	// create zip writer
	zipWriter := zip.NewWriter(destZip)
	defer utils.Close(zipWriter)

	// get files
	source := filepath.Join(utils.TmpDir, z.Job.ID)
	jars, err := ioutil.ReadDir(source)
	utils.ErrCheck(err)

	for _, jar := range jars {
		jarFile, err := os.Open(filepath.Join(source, jar.Name()))
		utils.ErrCheck(err)
		defer utils.Close(jarFile)

		info, err := jarFile.Stat()
		utils.ErrCheck(err)

		header, err := zip.FileInfoHeader(info)
		utils.ErrCheck(err)

		header.Method = zip.Deflate

		writer, err := zipWriter.CreateHeader(header)
		utils.ErrCheck(err)

		_, err = io.Copy(writer, jarFile)
		utils.ErrCheck(err)
	}
}
