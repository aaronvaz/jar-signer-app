package utils

import (
	"flag"
	"io"
	"os"
	"path/filepath"
	"runtime/debug"

	"gitlab.com/aaronvaz/jar-signer-app/pkg/loggers"
)

const (
	// JarSignerExe is the path to the jarsigner binary
	JarSignerExe = "jarsigner"

	// SessionCookieName refers to the name of the session cookie that the application creates
	SessionCookieName = "sessionId"
)

// Args holds all the cli flags the application is configured with
type Args struct {
	Port       string
	MaxJobs    int
	Keystore   string
	Storepass  string
	Keypass    string
	Digestalg  string
	Alias      string
	WorkingDir string
}

// CmdArgs returns args to be passed to the jarsigner binary
func (a *Args) CmdArgs(jarName string) []string {
	return append([]string{},
		"-keystore", a.Keystore,
		"-storepass", a.Storepass,
		"-keypass", a.Keypass,
		"-digestalg", a.Digestalg,
		jarName, a.Alias)
}

// parseArgs parses allflags and create args instance
func parseArgs() *Args {
	toRet := &Args{}

	flag.StringVar(&toRet.Port, "port", "8000", "Port to bind the server to")
	flag.IntVar(&toRet.MaxJobs, "maxJobs", 10, "Max number of jobs to execute concurrently")
	flag.StringVar(&toRet.Keystore, "keystore", "", "Path to Java keystore")
	flag.StringVar(&toRet.Storepass, "storepass", "", "password for keystore integrity")
	flag.StringVar(&toRet.Keypass, "keypass", "", "password for private key")
	flag.StringVar(&toRet.Digestalg, "digestalg", "", "name of digest algorithm")
	flag.StringVar(&toRet.Alias, "alias", "", "alias for the keystore")
	flag.Parse()

	toRet.WorkingDir = getWorkingDir()

	return toRet
}

func getWorkingDir() string {
	exe, err := os.Executable()
	if err != nil {
		loggers.AppLogger.Fatalln(err)
	}
	return filepath.Dir(exe)
}

// Close calls the close method on a io.Closer
// if an error occurs during the close call it panics the err
func Close(c io.Closer) {
	ErrCheck(c.Close())
}

// ErrCheck checks the error variable
// if the err is not nil it calls panic
func ErrCheck(err error) {
	if err != nil {
		panic(err)
	}
}

// CatchErr recovers from a panic and logs it
// it should be used in a defer call
func CatchErr() {
	if r := recover(); r != nil {
		loggers.AppLogger.Println(r, "\n", string(debug.Stack()))
	}
}

// AppArgs is the program flags singleton
var AppArgs = parseArgs()

var (
	// TmpDir points to a temporary directory the application will use
	TmpDir = filepath.Join(AppArgs.WorkingDir, "tmp")

	// DownloadDir points to the directory where finished zips will stored
	DownloadDir = filepath.Join(AppArgs.WorkingDir, "download")
)
