package ws

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/aaronvaz/jar-signer-app/pkg/utils"
)

// DowloadQueue is the job queue for
var DowloadQueue = make(chan *DownloadJob)

// DownloadHandler is a http handler that serves the created zips
func DownloadHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/zip")
	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename='%s'", filepath.Base(r.RequestURI)))
	http.ServeFile(w, r, "."+r.RequestURI)
}

// DownloadResponse represents the json payload that will contain a list of zip 's for the client to download
type DownloadResponse struct {
	MessageType string `json:"type"`
	Latest      Zip    `json:"latest"`
	Other       []Zip  `json:"others"`
}

// Zip represents the zip information in the download response payload
type Zip struct {
	Name string `json:"name"`
	Path string `json:"path"`
}

// DownloadJob represents a request for download links of signed jar zips
type DownloadJob struct {
	ID string
}

// Run executes the download job
func (dj *DownloadJob) Run() {
	defer utils.CatchErr()

	// get all zips
	source := filepath.Join(utils.DownloadDir, dj.ID)
	zips, err := ioutil.ReadDir(source)
	utils.ErrCheck(err)

	zipLinks := dj.createDownloadPaths(zips)

	response := &DownloadResponse{
		MessageType: "download",
		Latest:      zipLinks[len(zipLinks)-1],
		Other:       zipLinks[:len(zipLinks)-1],
	}

	MessageService.SendMessage(dj.ID, response)
}

func (dj *DownloadJob) createDownloadPaths(files []os.FileInfo) []Zip {
	base := strings.Replace(utils.DownloadDir, ".", "", 1)
	baseDownloadPath := filepath.Join(base, dj.ID)

	var zips []Zip
	for _, file := range files {
		zip := Zip{Name: file.Name(), Path: filepath.Join(baseDownloadPath, file.Name())}
		zips = append(zips, zip)
	}
	return zips
}
