package ws

import (
	"net/http"
	"sync"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"

	"gitlab.com/aaronvaz/jar-signer-app/pkg/loggers"
	"gitlab.com/aaronvaz/jar-signer-app/pkg/utils"
)

var (
	// MessageService exports the service to other packages
	MessageService = &messageService{make(map[*messageServiceID]*websocket.Conn)}

	// MessageQueue is used to queue message requests
	MessageQueue = make(chan *LogMessagesJob)
)

// LogMessagesJob represents a request send log messages to the client
type LogMessagesJob struct {
	ID      string
	Message *LogMessages
}

// LogMessages hold the relevant log message that will be send to the client
type LogMessages struct {
	MessageType string `json:"type"`
	JarName     string `json:"name"`
	Message     string `json:"message"`
}

var websocketService = websocket.Upgrader{
	WriteBufferSize: 1024,
	ReadBufferSize:  1024,
}

// MessageHandler is a http handler that is used to send messages to the clients
// the communication occurs using a websocket connection
func MessageHandler(w http.ResponseWriter, r *http.Request) {
	connection, err := websocketService.Upgrade(w, r, nil)
	utils.ErrCheck(err)

	MessageService.addClient(mux.Vars(r)["id"], connection)
}

// LogMessageService is the service that will be used to send log messages to the clients over the websocket connection
type messageService struct {
	clients map[*messageServiceID]*websocket.Conn
}

type messageServiceID struct {
	sync.Mutex
	id string
}

func (ms *messageService) addClient(id string, connection *websocket.Conn) {
	// check if connection exists
	extConnID, extConn := ms.getClient(id)

	// close old connection and remove from map
	if extConn != nil {
		loggers.AppLogger.Printf("connection for client %s already exists, deleting old connection", extConnID.id)
		utils.ErrCheck(extConn.Close())
		delete(ms.clients, extConnID)
	}

	msID := &messageServiceID{id: id}
	ms.clients[msID] = connection
}

func (ms *messageService) getClient(id string) (*messageServiceID, *websocket.Conn) {
	for key, value := range ms.clients {
		if key.id == id {
			return key, value
		}
	}
	return new(messageServiceID), nil
}

func (ms *messageService) SendMessage(id string, message interface{}) {
	clientID, client := ms.getClient(id)

	defer utils.CatchErr()

	if clientID.id == "" || client == nil {
		panic("No client found with ID: " + id)
	}

	if client != nil {
		clientID.Lock()
		defer clientID.Unlock()

		err := client.WriteJSON(message)
		utils.ErrCheck(err)

	} else {
		delete(ms.clients, clientID)
	}
}
