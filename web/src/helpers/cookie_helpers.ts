export default class CookieHelper {
  public static getSessionId(): string {
    return document.cookie
      .split(";")
      .filter(cookie => cookie.indexOf("sessionId") === 0)
      .map(cookie => cookie.split("=", 2)[1])[0];
  }
}