export interface Message {
    type: string
}

export interface LogMessage extends Message {
    name: string
    message: string
}

export interface DownloadMessage extends Message {
    latest: Zip
    others: Array<Zip>
}

export interface Zip {
    name: string
    path: string
}