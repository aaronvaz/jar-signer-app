import { Message } from "../model/json_models"
import CookieHelper from "../helpers/cookie_helpers";

export default class MessageService {
    private static _instance: MessageService;

    private ws: WebSocket;
    private static handlers: Map<string, Function> = new Map();

    private constructor() {
        this.ws = new WebSocket(`ws://${window.location.host}/payload/${CookieHelper.getSessionId()}`);
        this.ws.onmessage = this.handle_message
    }

    public static get instance() {
        return this._instance || (this._instance = new this());
    }

    private handle_message(payload: MessageEvent): void {
        const message: Message = JSON.parse(payload.data);
        if (MessageService.handlers.get(message.type)) {
            const func = MessageService.handlers.get(message.type);
            if (func) {
                func.call(this, payload.data)
            }
        }
    }

    register_handlers(type: string, hander_func: Function): void {
        MessageService.handlers.set(type, hander_func)
    }
}